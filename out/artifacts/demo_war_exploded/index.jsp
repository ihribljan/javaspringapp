<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*" %>



<html>
<head>
  <title>SELECT Operation</title>

</head>
<body>

<sql:setDataSource var="snapshot" driver="com.mysql.cj.jdbc.Driver"
                   url="jdbc:mysql://localhost:3306/FootballTournament?useSSL=false&serverTimezone=UTC"
                   user="root"  password="5Ud3snsN"/>

<sql:query dataSource="${snapshot}" var="result">
  SELECT * from players;
</sql:query>

<table border="1" width="100%" class="table table-hover table-inverse">
  <tr>
    <th>Id</th>
    <th>First name</th>
    <th>Last Name</th>
  </tr>
  <c:forEach var="row" items="${result.rows}">
    <tr>
      <td><c:out value="${row.id}"/></td>
      <td><c:out value="${row.firstName}"/></td>
      <td><c:out value="${row.lastName}"/></td>
    </tr>
  </c:forEach>
</table>

</body>
</html>