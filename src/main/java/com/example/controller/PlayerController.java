package com.example.controller;

import com.example.model.Player;
import com.example.service.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/")
public class PlayerController {

    @Autowired
    private PlayerService playerService;

    @GetMapping("/player")
    public List<Player> get() {
        return playerService.getAll();
    }

    @PostMapping("/player/create")
    public Player create(@RequestBody Player newPlayer) {
        return playerService.save(newPlayer);
    };

    @PutMapping("/player/update")
    public Player update(@RequestBody Player newPlayer) {
        return playerService.update(newPlayer);
    }

    @DeleteMapping("/player/delete/{id}")
    public void delete(@PathVariable int id) {
        playerService.deleteById(id);
        System.out.println("Id: " + id);
    }
}
