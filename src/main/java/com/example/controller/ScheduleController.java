package com.example.controller;

import com.example.model.Schedule;
import com.example.service.ScheduleService;
import com.example.service.StandingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/")
public class ScheduleController {

    @Autowired
    private ScheduleService scheduleService;

    @Autowired
    private StandingsService standingsService;

    @GetMapping("/schedule")
    public List<Schedule> get() {
        List<Schedule> list = scheduleService.getAll();
        return list;
    }

    @PostMapping("/schedule/create")
    public Schedule create(@RequestBody Schedule newSchedule) {
        standingsService.update(newSchedule);
        return scheduleService.save(newSchedule);
    }

    @PutMapping("/schedule/update")
    public Schedule update(@RequestBody Schedule newSchedule) {
        standingsService.update(newSchedule);
        return scheduleService.update(newSchedule);
    }

    @DeleteMapping("/schedule/delete/{id}")
    public void delete(@PathVariable int id) {
        standingsService.updateAfterDelete(id);
        scheduleService.deleteById(id);
        System.out.println("Id: " + id);
    }
}