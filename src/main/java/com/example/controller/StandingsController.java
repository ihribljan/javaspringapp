package com.example.controller;

import com.example.model.Schedule;
import com.example.model.Standings;
import com.example.service.StandingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/")
public class StandingsController {

    @Autowired
    private StandingsService standingsService;

    @GetMapping("/standings")
    public List<Standings> get() {
        return standingsService.getAll();
    }

    @PutMapping("/standings/update")
    public Standings update(@RequestBody Schedule schedule) {
        return standingsService.update(schedule);
    }
}
