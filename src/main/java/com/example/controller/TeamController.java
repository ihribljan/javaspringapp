package com.example.controller;

import com.example.model.Player;
import com.example.model.Team;
import com.example.service.PlayerService;
import com.example.service.StandingsService;
import com.example.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.temporal.Temporal;
import java.util.List;

@CrossOrigin("*")
@RestController
@RequestMapping("/")
public class TeamController {

    @Autowired
    private TeamService teamService;

    @GetMapping("/team")
    public List<Team> get() {
        List<Team> list = teamService.getAll();
        return list;
    }

    @PostMapping("/team/create")
    public Team create(@RequestBody Team newTeam) {
        teamService.save(newTeam);
        return newTeam;
    };

    @PutMapping("/team/update")
    public Team update(@RequestBody Team newTeam) {
        return teamService.update(newTeam);
    }

    @DeleteMapping("/team/delete/{id}")
    public void delete(@PathVariable int id) {
        teamService.deleteById(id);
        System.out.println("Id: " + id);
    }
}
