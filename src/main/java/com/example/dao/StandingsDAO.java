package com.example.dao;

import com.example.model.Standings;
import com.example.repo.StandingsRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public class StandingsDAO implements JpaRepository<Standings, Integer> {

    @Autowired
    private StandingsRepo standingsRepo;

    @Override
    public List<Standings> findAll() {
        return standingsRepo.findAll();
    }

    @Override
    public List<Standings> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<Standings> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<Standings> findAllById(Iterable<Integer> integers) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Integer integer) {

    }

    @Override
    public void delete(Standings entity) {

    }

    @Override
    public void deleteAll(Iterable<? extends Standings> entities) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends Standings> S save(S entity) {
        return standingsRepo.save(entity);
    }

    @Override
    public <S extends Standings> List<S> saveAll(Iterable<S> entities) {
        return null;
    }

    @Override
    public Optional<Standings> findById(Integer integer) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Integer integer) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends Standings> S saveAndFlush(S entity) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<Standings> entities) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public Standings getOne(Integer integer) {
        return null;
    }

    @Override
    public <S extends Standings> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends Standings> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends Standings> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends Standings> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends Standings> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends Standings> boolean exists(Example<S> example) {
        return false;
    }
}
