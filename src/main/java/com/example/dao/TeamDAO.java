package com.example.dao;

import com.example.model.Team;
import com.example.repo.TeamRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public class TeamDAO implements JpaRepository<Team, Integer> {

    @Autowired
    private TeamRepo teamRepo;

    @Override
    public List<Team> findAll() {
        return teamRepo.findAll();
    }

    @Override
    public void deleteById(Integer integer) {
        teamRepo.deleteById(integer);
    }

    @Override
    public <S extends Team> S save(S entity) {
        return teamRepo.save(entity);
    }

    @Override
    public List<Team> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<Team> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<Team> findAllById(Iterable<Integer> integers) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void delete(Team entity) {

    }

    @Override
    public void deleteAll(Iterable<? extends Team> entities) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends Team> List<S> saveAll(Iterable<S> entities) {
        return null;
    }

    @Override
    public Optional<Team> findById(Integer integer) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Integer integer) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends Team> S saveAndFlush(S entity) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<Team> entities) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public Team getOne(Integer integer) {
        return null;
    }

    @Override
    public <S extends Team> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends Team> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends Team> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends Team> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends Team> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends Team> boolean exists(Example<S> example) {
        return false;
    }
}
