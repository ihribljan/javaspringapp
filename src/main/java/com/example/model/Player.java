package com.example.model;

import javax.persistence.*;

@Entity
@Table(name = "players")
public class Player {

    @Id
    @Column(name = "Id", nullable = false)
    private int id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "teams_id")
    private int teamsId;
/*
    @ManyToOne(optional = false)
    @JoinColumn(name = "teams_id", insertable = false, updatable = false)
    private Team team;

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }
*/
    public Player() {
    }

    public Player(String _firstName, String _lastName, int _teamsId) {
        this.firstName = _firstName;
        this.lastName = _lastName;
        this.teamsId = _teamsId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getTeamsId() {
        return teamsId;
    }

    public void setTeamsId(int teamsId) {
        this.teamsId = teamsId;
    }

    @Override
    public String toString() {
        return "Player{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", teamsId=" + teamsId +
                '}';
    }

}
