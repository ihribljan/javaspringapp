package com.example.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "schedules")
public class Schedule {

    @Id
    @Column(name = "id")
    private int id;

    public Schedule() {
    }

    @Column(name = "date_and_time")
    private LocalDateTime dateAndTime;

    @Column(name = "teams_id_home")
    private int teamsIdHome;

    @Column(name = "teams_id_guest")
    private int teamsIdGuest;

    @Column(name = "goals_home")
    private int goalsHome;

    @Column(name = "goals_guest")
    private int goalsGuest;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDateTime getDateAndTime() {
        return dateAndTime;
    }

    public void setDateAndTime(LocalDateTime dateAndTime) {
        this.dateAndTime = dateAndTime;
    }

    public int getTeamsIdHome() {
        return teamsIdHome;
    }

    public void setTeamsIdHome(int teamsIdHome) {
        this.teamsIdHome = teamsIdHome;
    }

    public int getTeamsIdGuest() {
        return teamsIdGuest;
    }

    public void setTeamsIdGuest(int teamsIdGuest) {
        this.teamsIdGuest = teamsIdGuest;
    }

    public int getGoalsHome() {
        return goalsHome;
    }

    public void setGoalsHome(int goalsHome) {
        this.goalsHome = goalsHome;
    }

    public int getGoalsGuest() {
        return goalsGuest;
    }

    public void setGoalsGuest(int goalsGuest) {
        this.goalsGuest = goalsGuest;
    }

    @Override
    public String toString() {
        return "Schedule{" +
                "id=" + id +
                ", dateAndTime=" + dateAndTime +
                ", teamsIdHome=" + teamsIdHome +
                ", teamsIdGuest=" + teamsIdGuest +
                ", goalsHome=" + goalsHome +
                ", goalsGuest=" + goalsGuest +
                '}';
    }
}
