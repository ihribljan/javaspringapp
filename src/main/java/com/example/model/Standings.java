package com.example.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name= "standings", schema = "footballTournament")
public class Standings {

    @Id
    @Column(name = "id")
    private int id;

    @Column(name = "team_id")
    private int teamId;

    @Column(name = "played")
    private int played;

    @Column(name = "won")
    private int won;

    @Column(name = "drawn")
    private int drawn;

    @Column(name = "lost")
    private int lost;

    @Column(name = "goals_for")
    private int goalsFor;

    @Column(name = "goals_against")
    private int goalsAgainst;

    @Column(name = "goals_diff")
    private int goalsDiff;

    @Column(name = "points")
    private int points;

    public Standings() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTeamId() {
        return teamId;
    }

    public void setTeamId(int teamId) {
        this.teamId = teamId;
    }

    public int getPlayed() {
        return played;
    }

    public void setPlayed(int played) {
        this.played = played;
    }

    public int getWon() {
        return won;
    }

    public void setWon(int won) {
        this.won = won;
    }

    public int getDrawn() {
        return drawn;
    }

    public void setDrawn(int drawn) {
        this.drawn = drawn;
    }

    public int getLost() {
        return lost;
    }

    public void setLost(int lost) {
        this.lost = lost;
    }

    public int getGoalsFor() {
        return goalsFor;
    }

    public void setGoalsFor(int goalsFor) {
        this.goalsFor = goalsFor;
    }

    public int getGoalsAgainst() {
        return goalsAgainst;
    }

    public void setGoalsAgainst(int goalsAgainst) {
        this.goalsAgainst = goalsAgainst;
    }

    public int getGoalsDiff() {
        return goalsDiff;
    }

    public void setGoalsDiff(int goalsDiff) {
        this.goalsDiff = goalsDiff;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    @Override
    public String toString() {
        return "Standings{" +
                "id=" + id +
                ", teamId=" + teamId +
                ", played=" + played +
                ", won=" + won +
                ", drawn=" + drawn +
                ", lost=" + lost +
                ", goalsFor=" + goalsFor +
                ", goalsAgainst=" + goalsAgainst +
                ", goalsDiff=" + goalsDiff +
                ", points=" + points +
                '}';
    }

    // prima teamId, a vraća id reda u tablici standings di se nalazi taj tim
    public int getIdFromTeamId(int teamId) {
        try {
            // create our mysql database connection
            String myDriver = "com.mysql.cj.jdbc.Driver";
            String myUrl = "jdbc:mysql://localhost:3306/FootballTournament?useSSL=false&serverTimezone=UTC";
            Class.forName(myDriver);
            Connection con = DriverManager.getConnection(myUrl, "root", "5Ud3snsN");

            String query = "SELECT id FROM standings WHERE team_id = " + teamId;

            // create the java statement
            Statement st = con.createStatement();

            // execute the query, and get a java resultset
            ResultSet rs = st.executeQuery(query);

            // iterate through the java resultset
            while (rs.next()) {
                return rs.getInt("id");
            }
            st.close();
        } catch (Exception e) {
            System.out.println("Error! " + e.getMessage());
        }
        return 0;
    }

    // prima id tima, vraća sve njegove podatke iz tablice standings
    public Standings getTeamData(int idTeam) {
        Standings standings = new Standings();
        try {
            // create our mysql database connection
            String myDriver = "com.mysql.cj.jdbc.Driver";
            String myUrl = "jdbc:mysql://localhost:3306/FootballTournament?useSSL=false&serverTimezone=UTC";
            Class.forName(myDriver);
            Connection conn = DriverManager.getConnection(myUrl, "root", "5Ud3snsN");

            String query = "SELECT * FROM standings WHERE team_id = " + idTeam;

            // create the java statement
            Statement st = conn.createStatement();

            // execute the query, and get a java resultset
            ResultSet rs = st.executeQuery(query);

            // iterate through the java resultset
            while (rs.next()) {
                standings.id = rs.getInt("id");
                standings.teamId = rs.getInt("team_id");
                standings.played = rs.getInt("played");
                standings.won = rs.getInt("won");
                standings.drawn = rs.getInt("drawn");
                standings.lost = rs.getInt("lost");
                standings.goalsFor = rs.getInt("goals_for");
                standings.goalsAgainst = rs.getInt("goals_against");
                standings.goalsDiff = rs.getInt("goals_diff");
                standings.points = rs.getInt("points");

                return standings;
            }
            st.close();
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return null;

    }

    // prima id schedule, vraća home, guest, goalsHome, goalsGuest
    public Schedule UpdAftDel(int idSchedule) {
        Schedule schedule = new Schedule();
        try {
            // create our mysql database connection
            String myDriver = "com.mysql.cj.jdbc.Driver";
            String myUrl = "jdbc:mysql://localhost:3306/FootballTournament?useSSL=false&serverTimezone=UTC";
            Class.forName(myDriver);
            Connection conn = DriverManager.getConnection(myUrl, "root", "5Ud3snsN");

            String query = "SELECT * FROM schedules WHERE id = " + idSchedule;

            // create the java statement
            Statement st = conn.createStatement();

            // execute the query, and get a java resultset
            ResultSet rs = st.executeQuery(query);

            // iterate through the java resultset
            while (rs.next()) {
                schedule.setId(rs.getInt("id"));
                schedule.setTeamsIdHome(rs.getInt("teams_id_home"));
                schedule.setTeamsIdGuest(rs.getInt("teams_id_guest"));
                schedule.setGoalsHome(rs.getInt("goals_home"));
                schedule.setGoalsGuest(rs.getInt("goals_guest"));

                return schedule;
            }
            st.close();
        } catch (Exception e) {
            System.out.println("Greška!: " + e.getMessage());
        }
        return null;

    }
}
