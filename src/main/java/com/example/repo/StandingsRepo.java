package com.example.repo;

import com.example.model.Standings;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StandingsRepo extends JpaRepository<Standings, Integer> {
}
