package com.example.service;

import com.example.model.Player;
import com.example.repo.PlayerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.util.List;

@Service
@Transactional
public class PlayerService {

    @Autowired
    private PlayerRepo playerRepo;

    @Autowired
    private EntityManagerFactory emf;

    public List<Player> getAll() {
        return playerRepo.findAll();
    }

    public Player save(Player newPlayer) {

        newPlayer.setFirstName(newPlayer.getFirstName());
        newPlayer.setLastName(newPlayer.getLastName());
        newPlayer.setTeamsId(newPlayer.getTeamsId());

        newPlayer = playerRepo.save(newPlayer);

        return newPlayer;
    }

    public Player update(Player newPlayer) {
        return playerRepo.findById(newPlayer.getId()).map(player -> {
            player.setFirstName(newPlayer.getFirstName());
            player.setLastName(newPlayer.getLastName());
            player.setTeamsId(newPlayer.getTeamsId());
            return playerRepo.save(player);
        }).orElseGet(() -> {
            newPlayer.setId(newPlayer.getId());
            return playerRepo.save(newPlayer);
        });
    }

    public void deleteById(int id) {
        playerRepo.deleteById(id);
    }
}

