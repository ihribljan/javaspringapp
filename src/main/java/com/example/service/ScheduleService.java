package com.example.service;

import com.example.model.Schedule;
import com.example.repo.ScheduleRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ScheduleService {

    @Autowired
    private ScheduleRepo scheduleRepo;

    public List<Schedule> getAll() {
        return scheduleRepo.findAll();
    }

    public Schedule save(Schedule newSchedule) {

        newSchedule.setDateAndTime(newSchedule.getDateAndTime());
        newSchedule.setTeamsIdHome(newSchedule.getTeamsIdHome());
        newSchedule.setTeamsIdGuest(newSchedule.getTeamsIdGuest());
        newSchedule.setGoalsHome(newSchedule.getGoalsHome());
        newSchedule.setGoalsGuest(newSchedule.getGoalsGuest());

        newSchedule = scheduleRepo.save(newSchedule);

        return newSchedule;
    }

    public Schedule update(Schedule newSchedule) {
        return scheduleRepo.findById(newSchedule.getId()).map(schedule -> {
            schedule.setTeamsIdHome(newSchedule.getTeamsIdHome());
            schedule.setTeamsIdGuest(newSchedule.getTeamsIdGuest());
            schedule.setGoalsHome(newSchedule.getGoalsHome());
            schedule.setGoalsGuest(newSchedule.getGoalsGuest());
            schedule.setDateAndTime(newSchedule.getDateAndTime());
            return scheduleRepo.save(schedule);
        }).orElseGet(() -> {
            newSchedule.setId(newSchedule.getId());
            return scheduleRepo.save(newSchedule);
        });
    }

    public void deleteById(int id) {
        scheduleRepo.deleteById(id);
    }
}
