package com.example.service;

import com.example.model.Schedule;
import com.example.model.Standings;
import com.example.model.Team;
import com.example.repo.StandingsRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
@Transactional
public class StandingsService {

    @Autowired
    private StandingsRepo standingsRepo;

    public List<Standings> getAll() {
        return standingsRepo.findAll();
    }

    public Standings update(Schedule newSchedule) {
        Standings standings = new Standings();

        Standings home = new Standings();
        Standings guest = new Standings();

        System.out.println("New schedule team home id: " + newSchedule.getTeamsIdHome());
        System.out.println("New schedule team guest id: " + newSchedule.getTeamsIdGuest());

        System.out.println("Id from team home id: " + home.getIdFromTeamId(newSchedule.getTeamsIdHome()));
        System.out.println("Id from team guest id: " + home.getIdFromTeamId(newSchedule.getTeamsIdGuest()));

        System.out.println(standings.getTeamData(newSchedule.getTeamsIdHome()));
        System.out.println(standings.getTeamData(newSchedule.getTeamsIdGuest()));

        home = standings.getTeamData(newSchedule.getTeamsIdHome());
        guest = standings.getTeamData(newSchedule.getTeamsIdGuest());

        home.setPlayed(home.getPlayed() + 1);
        guest.setPlayed(guest.getPlayed() + 1);

        if(newSchedule.getGoalsHome() > newSchedule.getGoalsGuest()) {
            home.setWon(home.getWon() + 1);
            home.setPoints(home.getPoints() + 3);
            guest.setLost(guest.getLost() + 1);
        }
        else if(newSchedule.getGoalsHome() == newSchedule.getGoalsGuest()) {
            home.setDrawn(home.getDrawn() + 1);
            home.setPoints(home.getPoints() + 1);
            guest.setDrawn(guest.getDrawn() + 1);
            guest.setPoints(guest.getPoints() + 1);
        }
        else {
            home.setLost(home.getLost() + 1);
            guest.setWon(guest.getWon() + 1);
            guest.setPoints(guest.getPoints() + 3);
        }

        home.setGoalsFor(home.getGoalsFor() + newSchedule.getGoalsHome());
        home.setGoalsAgainst(home.getGoalsAgainst() + newSchedule.getGoalsGuest());
        home.setGoalsDiff(home.getGoalsDiff() + newSchedule.getGoalsHome() - newSchedule.getGoalsGuest());

        guest.setGoalsFor(guest.getGoalsFor() + newSchedule.getGoalsGuest());
        guest.setGoalsAgainst(guest.getGoalsAgainst() + newSchedule.getGoalsHome());
        guest.setGoalsDiff(guest.getGoalsDiff() + newSchedule.getGoalsGuest() - newSchedule.getGoalsHome());

        System.out.println("home: " + home.getId() + " " + home.getPlayed() + " " + home.getWon() + " " +
                home.getDrawn() + " " + home.getLost() + " " + home.getGoalsFor() + " " +
                home.getGoalsAgainst() + " " + home.getGoalsDiff() + " " + home.getPoints());

        System.out.println("guest: " + guest.getId() + " " + guest.getPlayed() + " " + guest.getWon() + " " +
                guest.getDrawn() + " " + guest.getLost() + " " + guest.getGoalsFor() + " " +
                guest.getGoalsAgainst() + " " + guest.getGoalsDiff() + " " + guest.getPoints());

        home = standingsRepo.save(home);
        guest = standingsRepo.save(guest);

        return home;
    }

    public void updateAfterDelete(int scheduleId) {
        Standings standings = new Standings();

        System.out.println("Home: " + standings.UpdAftDel(scheduleId).getTeamsIdHome());
        System.out.println("Guest: " + standings.UpdAftDel(scheduleId).getTeamsIdGuest());
        System.out.println("Goals home: " + standings.UpdAftDel(scheduleId).getGoalsHome());
        System.out.println("Goals guest: " + standings.UpdAftDel(scheduleId).getGoalsGuest());

        Schedule schedule = standings.UpdAftDel(scheduleId);

        System.out.println("Home: " + schedule.getTeamsIdHome());
        System.out.println("Guest: " + schedule.getTeamsIdGuest());

        Standings teamHome = standings.getTeamData(schedule.getTeamsIdHome());
        Standings teamGuest = standings.getTeamData(schedule.getTeamsIdGuest());

        System.out.println("Teams: " + teamHome + " - " + teamGuest);

        teamHome.setPlayed(teamHome.getPlayed() - 1);
        teamGuest.setPlayed(teamGuest.getPlayed() - 1);

        teamHome.setGoalsFor(teamHome.getGoalsFor() - schedule.getGoalsHome());
        teamGuest.setGoalsFor(teamGuest.getGoalsFor() - schedule.getGoalsGuest());

        teamHome.setGoalsAgainst(teamHome.getGoalsAgainst() - schedule.getGoalsGuest());
        teamGuest.setGoalsAgainst(teamGuest.getGoalsAgainst() - schedule.getGoalsHome());

        teamHome.setGoalsDiff(teamHome.getGoalsFor() - teamHome.getGoalsAgainst());
        teamGuest.setGoalsDiff(teamGuest.getGoalsFor() - teamGuest.getGoalsAgainst());

        if(schedule.getGoalsHome() > schedule.getGoalsGuest()) {
            teamHome.setWon(teamHome.getWon() - 1);
            teamHome.setPoints(teamHome.getPoints() - 3);
            teamGuest.setLost(teamGuest.getLost() - 1);
        }
        else if(schedule.getGoalsHome() == schedule.getGoalsGuest()) {
            teamHome.setDrawn(teamHome.getDrawn() - 1);
            teamHome.setPoints(teamHome.getPoints() - 1);
            teamGuest.setDrawn(teamGuest.getDrawn() - 1);
            teamGuest.setPoints(teamGuest.getPoints() - 1);
        } else {
            teamHome.setLost(teamHome.getLost() - 1);
            teamGuest.setWon(teamGuest.getWon() - 1);
            teamGuest.setPoints(teamGuest.getPoints() - 3);
        }

        teamHome = standingsRepo.save(teamHome);
        teamGuest = standingsRepo.save(teamGuest);
    }
}
