package com.example.service;

import com.example.model.Standings;
import com.example.model.Team;
import com.example.repo.StandingsRepo;
import com.example.repo.TeamRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class TeamService {

    @Autowired
    private TeamRepo teamRepo;

    public List<Team> getAll() {
        return teamRepo.findAll();
    }

    public Team save(Team newTeam) {
        newTeam = teamRepo.save(newTeam);
        return newTeam;
    }

    public Team update(Team newTeam) {
        return teamRepo.findById(newTeam.getId()).map(team -> {
            team.setName(newTeam.getName());
            return teamRepo.save(team);
        }).orElseGet(() -> {
            newTeam.setId(newTeam.getId());
            return teamRepo.save(newTeam);
        });
    }

    public void deleteById(int id) {
        teamRepo.deleteById(id);
    }

}
